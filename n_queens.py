def find_positions(n):
    column = set()
    posDiag = set()
    negDiag = set()
    result = []

    # create a board
    board = [[0] * n for i in range(n)]

    def backtrack(row):
        # base case
        if row == n:
            copy = [r for r in board]
            print("copy", copy)
            temp = []
            for pos in copy:
                idx = pos.index(1)
                temp.append(idx)
            result.append(temp)
            return

        for col in range(n):
            if col in column or (row + col) in posDiag or (row - col) in negDiag:
                continue

            column.add(col)
            posDiag.add(row + col)
            negDiag.add(row - col)
            board[row][col] = 1

            backtrack(row + 1)

            column.remove(col)
            posDiag.remove(row + col)
            negDiag.remove(row - col)
            board[row][col] = 0

    backtrack(0)
    print("result", result)
    return result


find_positions(4)
